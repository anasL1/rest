package com.ps.rest.exception;

import java.time.LocalDate;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class MyExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionDetail> handleAllException(Exception ex, WebRequest request) {
		return new ResponseEntity<ExceptionDetail>(new ExceptionDetail(LocalDate.now(), ex.getMessage(),
				request.getDescription(false)),
				HttpStatus.INTERNAL_SERVER_ERROR);

	}
	@ExceptionHandler(DepartmentNotFoundException.class)
	public final ResponseEntity<ExceptionDetail> handleUserNotFoundException(Exception ex, WebRequest request) {
		return new ResponseEntity<ExceptionDetail>(new ExceptionDetail(LocalDate.now(), ex.getMessage(),
				request.getDescription(false)),
				HttpStatus.NOT_FOUND);
		
	}

}
