package com.ps.rest.exception;

import java.time.LocalDate;

public class ExceptionDetail {

	public LocalDate date;
	public String message;
	public String uri;

	public ExceptionDetail(LocalDate date, String message, String uri) {
		super();
		this.date = date;
		this.message = message;
		this.uri= uri;
	}

}
