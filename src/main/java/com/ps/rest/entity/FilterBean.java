package com.ps.rest.entity;

import com.fasterxml.jackson.annotation.JsonFilter;

@JsonFilter("SomeFilter")
public class FilterBean {
	private String a;
	private String b;
	private String c;
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public String getB() {
		return b;
	}
	public void setB(String b) {
		this.b = b;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public FilterBean(String a, String b, String c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}
	public FilterBean(String b, String c) {
		super();
		this.b = b;
		this.c = c;
	}
	public FilterBean() {
		super();
	}
	

}
