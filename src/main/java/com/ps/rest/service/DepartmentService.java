package com.ps.rest.service;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ps.rest.entity.Department;
import com.ps.rest.entity.Post;
import com.ps.rest.repo.DepartmentRepository;

@Service
public class DepartmentService {

	@Autowired
	DepartmentRepository departmentRepository;

	@Transactional
	public Department saveDepartment(Department department) {
		department.setName("asdf456");
		department.setPosts(Arrays.asList(new Post()));
		department.getPosts().stream().forEach(p->p.setDepartment(department));
		departmentRepository.save(department);
		return department;
	}

	@Transactional
	public Optional<Department> getDepartmentById(Integer dId) {
		Optional<Department> department = departmentRepository.findById(dId);
		department.get().setPosts(Arrays.asList(new Post()));
		try {
			departmentRepository.save(department.get());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return department;
	}
}
