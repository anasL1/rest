package com.ps.rest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/version")
public class VersionControl {

	
	//PARAMETER Versioning
	@GetMapping(params = "version=1")
	public String abc() {
		return "first";
	}
	@GetMapping(params = "version=2")
	public String abcdef() {
		return "second";
	}
	
	//HEADER Versioning
	@GetMapping(headers   = "version=1")
	public String abcheader() {
		return "first header";
	}
	@GetMapping(headers  = "version=2")
	public String abcdefheader() {
		return "second header";
	}
	//MIME Versioning
	@GetMapping(produces = "application/ps-v1+json")
	public String abcproduces() {
		return "first produces";
	}
	@GetMapping(produces = "application/ps-v2+json")
	public String abcdefproduces() {
		return "second produces";
	}
	
}
