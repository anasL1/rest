package com.ps.rest.controller;

import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
	@Autowired
	MessageSource messageSource;

	@GetMapping("/asdf")
	public String getDepartments(@RequestHeader(name = "Accept-Language", required = false) Locale locale) {
		System.out.println("hello");
		try {
			messageSource.getMessage("hello.msg", null,locale);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return messageSource.getMessage("hello.msg", null, "Defualt Message", locale);
	}
	@GetMapping("/")
	public String getDepartments() {
		return messageSource.getMessage("hello.msg", null,"Default",LocaleContextHolder.getLocale());
	}

}
