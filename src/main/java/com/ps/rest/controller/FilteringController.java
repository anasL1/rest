package com.ps.rest.controller;

import com.fasterxml.jackson.databind.ser.BeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ps.rest.entity.Department;
import com.ps.rest.entity.FilterBean;
import com.ps.rest.exception.DepartmentNotFoundException;
import com.ps.rest.service.DepartmentService;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.converter.json.MappingJacksonValue;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/filter")
public class FilteringController {

	@Autowired
	DepartmentService departmentService;

	@GetMapping("/")
	public MappingJacksonValue getDepartments() {
		System.out.println("asdf");
		FilterBean filterBean = new FilterBean();
		MappingJacksonValue jacksonValue = new MappingJacksonValue(filterBean);
		SimpleBeanPropertyFilter beanPropertyFilter = SimpleBeanPropertyFilter.filterOutAllExcept("a", "b");
		FilterProvider filter = new SimpleFilterProvider().addFilter("SomeFilter", beanPropertyFilter);
		jacksonValue.setFilters(filter);
		return jacksonValue;
	}

	@GetMapping("/{id}")
	public EntityModel<Department> getDepartmentById(@PathVariable("id") int id) {
		System.out.println("hello" + id);

		Optional<Department> department = departmentService.getDepartmentById(id);
		if (department.isEmpty())
			throw new DepartmentNotFoundException("id-" + id);
		EntityModel<Department> model = EntityModel.of(department.get());
		WebMvcLinkBuilder linkToUsers = linkTo(methodOn(this.getClass()).getDepartments());
		model.add(linkToUsers.withRel("all-departments"));
		return model;
	}

	@PostMapping
	public Department saveDepartment(@Valid @RequestBody Department department) {

		return departmentService.saveDepartment(department);

	}
}
