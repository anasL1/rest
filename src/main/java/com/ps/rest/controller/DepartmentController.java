package com.ps.rest.controller;

import com.ps.rest.entity.Department;
import com.ps.rest.entity.Post;
import com.ps.rest.exception.DepartmentNotFoundException;
import com.ps.rest.service.DepartmentService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/departments")
public class DepartmentController {

	@Autowired
	DepartmentService departmentService;
	@GetMapping("/")
	public Department getDepartments() {
		System.out.println("hello");
		return new Department();
	}

	@GetMapping("/{id}")
	public EntityModel<Department>  getDepartmentById(@PathVariable("id") int id) {
		System.out.println("hello" + id);
		
		Optional<Department> department= departmentService.getDepartmentById(id);
		if(department.isEmpty())
			throw new DepartmentNotFoundException("id-"+id);
		EntityModel<Department> model = EntityModel.of(department.get());
		WebMvcLinkBuilder linkToUsers= linkTo(methodOn( this.getClass()).getDepartments());
		model.add(linkToUsers.withRel("all-departments"));
		return model;
	}
	
	@PostMapping
	public Department saveDepartment(@Valid @RequestBody Department department) {
		
		return departmentService.saveDepartment(department);
		
	}
	
	@GetMapping("/{id}/post")
	public Collection<Post>  getAllPostByDepartmentId(@PathVariable("id") int id) {
		System.out.println("hello" + id);
		
		Optional<Department> department= departmentService.getDepartmentById(id);
		if(department.isEmpty())
			throw new DepartmentNotFoundException("id-"+id);
		department.get().setPosts(Arrays.asList(new Post()));
		return department.get().getPosts();
	}
}
